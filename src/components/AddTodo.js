const AddTodo = ({ onAdd }) => {
    return (<>
        <input id="add-todo" />
        <label htmlFor="add-todo">{"Add New Todo"}</label>
    </>);
}

export default AddTodo;
